'use strict';

const Hapi = require('hapi');
var Boom = require('boom')
// add timestamps in front of log messages
require("console-stamp")(console, {
    pattern: "dd/mm/yyyy HH:MM:ss.l"
});

var apihostname = process.env.HOSTNAME || 'localhost',
    apihostport = process.env.PORT || 8080

// Create a server with a host and port
const server = new Hapi.Server();

server.connection({
    host: apihostname,
    port: apihostport
});

server.route({
    method: 'POST',
    path: '/geolocations',
    handler: function (request, reply) {
        console.log("+++ IN POST /geolocations");

        console.log('request.headers:', request.headers);
        console.log('request.payload:', request.payload);

        return reply({
            "statusCode": "200",
            "message": `POST /geolocations OK`
        })
    }
});

// Start the server
server.start((err) => {
    // console.error(err);
    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});
